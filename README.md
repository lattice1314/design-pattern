# Java设计模式

## 📢介绍 
学习Java设计模式时写的简单的java代码，以及为了便于理解和加深印象所画的UML图

## 🍺创建型模式
这类模式提供创造对象的机制，能够提升已有代码的灵活性和可复用性
1. [工厂方法](./src/main/java/factory/README.md)![工厂方法.png](img/factory.png)
2. [抽象工厂](./src/main/java/abstract_factory/README.md)![抽象工厂.png](img/abstract_factory.png)
3. [建造者](./src/main/java/builder/README.md)![建造者.png](img/builder.png)
4. [原型](./src/main/java/prototype/README.md)![原型.png](img/prototype.png)
5. [单例](./src/main/java/singleton/README.md)![单例.png](img/singleton.png)

## 🍗结构性模式
这类模式介绍如何将对象和类组装成较大的结构，并同时保持结构的灵活和高效
1. [适配器]()![适配器.png](img/adapter.png)
2. [桥接]()![桥接.png](img/bridge.png)
3. [组合]()![组合.png](img/composite.png)
4. [装饰]()![装饰.png](img/decorator.png)
5. [外观]()![外观.png](img/facade.png)
6. [享元]()![享元.png](img/flyweight.png)
7. [代理]()![代理.png](img/proxy.png)

## 🦞行为模式
这类模式负责对象间的高效沟通和职责委派
1. [责任链]()![责任链.png](img/chainOfResponsibility.png)
2. [命令]()![命令.png](img/command.png)
3. [迭代器]()![迭代器.png](img/iterator.png)
4. [中介者]()![中介者.png](img/mediator.png)
5. [备忘录]()![备忘录.png](img/memento.png)
6. [观察者]()![观察者.png](img/observer.png)
7. [状态]()![状态.png](img/state.png)
8. [策略]()![策略.png](img/strategy.png)
9. [模板]()![模板.png](img/template.png)
10. [访问者]()![访问者.png](img/visitor.png)
