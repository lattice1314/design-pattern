package factory;

import factory.logistics.transport.Transport;
import factory.logistics.transport.impl.Ship;
import factory.logistics.transport.impl.Trunk;
import org.junit.Test;

public class ApiTest {

    @Test
    public void testLogistics(){
        Transport transport;
        int i = (int) (Math.random() * 10);

        if (i < 5){
            transport = new Trunk();
        }else{
            transport = new Ship();
        }

        transport.deliver();
    }
}
