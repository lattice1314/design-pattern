package abstract_factory;

import abstract_factory.client.Client;
import abstract_factory.factory.FurnitureFactory;
import abstract_factory.factory.impl.ArtDecoFurnitureFactory;
import abstract_factory.factory.impl.ModernFurnitureFactory;
import abstract_factory.factory.impl.VictorianFurnitureFactory;
import org.junit.Test;

public class ApiTest {

    @Test
    public void testFurniture(){
        Client client;
        FurnitureFactory factory;
        int i = (int) (Math.random() * 10);

        if (i > 0 && i <= 3){
            factory = new ArtDecoFurnitureFactory();
            client = new Client(factory);
        }else if(i > 3 && i < 6 ){
            factory = new ModernFurnitureFactory();
            client = new Client(factory);
        }else {
            factory = new VictorianFurnitureFactory();
            client = new Client(factory);
        }

        client.buy();
    }
}
