package builder;

import builder.car.builders.impl.CarBuilder;
import builder.car.builders.impl.CarManualBuilder;
import builder.car.cars.Car;
import builder.car.cars.Manual;
import builder.car.director.Director;
import org.junit.Test;

public class ApiTest {

    @Test
    public void TestCar(){
        Director director = new Director();

        CarBuilder builder = new CarBuilder();
        director.constructSportsCar(builder);

        Car car = builder.getResult();
        System.out.println("Car built:\n" + car.getCarType());

        CarManualBuilder manualBuilder = new CarManualBuilder();

        director.constructSportsCar(manualBuilder);
        Manual carManual = manualBuilder.getResult();
        System.out.println("\nCar manual built:\n" + carManual.print());
    }
}
