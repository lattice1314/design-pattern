package factory.logistics.factory;

import factory.logistics.transport.Transport;
import factory.logistics.transport.impl.Trunk;

public class RoadLogistics extends Logistics {

    @Override
    public Transport createTransport(){
        return new Trunk();
    }
}
