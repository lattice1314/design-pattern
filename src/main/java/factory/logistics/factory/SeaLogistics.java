package factory.logistics.factory;

import factory.logistics.transport.Transport;
import factory.logistics.transport.impl.Ship;

public class SeaLogistics extends Logistics{

    @Override
    public Transport createTransport(){
        return new Ship();
    }
}
