package factory.logistics.factory;

import factory.logistics.transport.Transport;

public abstract class Logistics {

    public void planDelivery(){
        Transport transport = createTransport();
        transport.deliver();
    }

    public abstract Transport createTransport();
}
