package factory.logistics.transport.impl;

import factory.logistics.transport.Transport;

public class Trunk implements Transport {

    @Override
    public void deliver() {
        System.out.println("在盒子中以陆路运输");
    }

}
