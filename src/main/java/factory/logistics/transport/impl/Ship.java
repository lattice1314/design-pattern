package factory.logistics.transport.impl;

import factory.logistics.transport.Transport;

public class Ship implements Transport {

    @Override
    public void deliver() {
        System.out.println("在集装箱中以海路运输");
    }

}
