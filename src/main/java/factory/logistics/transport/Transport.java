package factory.logistics.transport;

public interface Transport {

    void deliver();

}
