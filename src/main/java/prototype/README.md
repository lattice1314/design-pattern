## 介绍
原型模式是一种创建型设计模式， 使你能够复制已有对象， 而又无需使代码依赖它们所属的类。

![img.png](img.png)

![Prototype](./prototype.png)

