package abstract_factory.client;

import abstract_factory.factory.FurnitureFactory;
import abstract_factory.furniture.Chair;
import abstract_factory.furniture.CoffeeTable;
import abstract_factory.furniture.Sofa;

public class Client {

    private Chair chair;
    private CoffeeTable coffeeTable;
    private Sofa sofa;

    public Client(FurnitureFactory factory){
        chair = factory.createChair();
        coffeeTable = factory.createCoffeeTable();
        sofa = factory.createSofa();
    }

    public void buy(){
        chair.sitOn();
        coffeeTable.layUp();
        sofa.lieDown();
    }
}
