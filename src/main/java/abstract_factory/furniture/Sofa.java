package abstract_factory.furniture;

public interface Sofa {

    void hasLegs();

    void lieDown();

}
