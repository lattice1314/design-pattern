package abstract_factory.furniture;

public interface Chair {

    void hasLegs();

    void sitOn();

}
