package abstract_factory.furniture.impl;

import abstract_factory.furniture.Chair;

public class ModernChair implements Chair {
    @Override
    public void hasLegs() {
        System.out.println("这是现代风格的椅子");
        System.out.println("这种椅子没有腿");
    }

    @Override
    public void sitOn() {
        System.out.println("这是现代风格的椅子");
        System.out.println("这种椅子可以坐");
    }
}
