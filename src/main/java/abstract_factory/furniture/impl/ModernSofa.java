package abstract_factory.furniture.impl;

import abstract_factory.furniture.Sofa;

public class ModernSofa implements Sofa {
    @Override
    public void hasLegs() {
        System.out.println("这是现代风格的沙发");
        System.out.println("这种沙发没有腿");
    }

    @Override
    public void lieDown() {
        System.out.println("这是装饰风艺术风格的沙发");
        System.out.println("这种沙发可以躺");
    }
}
