package abstract_factory.furniture.impl;

import abstract_factory.furniture.CoffeeTable;

public class ModernCoffeeTable implements CoffeeTable {
    @Override
    public void hasLegs() {
        System.out.println("这是现代风格的茶几");
        System.out.println("这种茶几有四条腿");
    }

    @Override
    public void layUp() {
        System.out.println("这是现代风格的茶几");
        System.out.println("这种茶几可以放东西");
    }
}
