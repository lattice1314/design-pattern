package abstract_factory.furniture.impl;

import abstract_factory.furniture.CoffeeTable;

public class VictorianCoffeeTable implements CoffeeTable {
    @Override
    public void hasLegs() {
        System.out.println("这是维多利亚风格的茶几");
        System.out.println("这种茶几有四条腿");
    }

    @Override
    public void layUp() {
        System.out.println("这是维多利亚风格的茶几");
        System.out.println("这种茶几可以放东西");
    }
}
