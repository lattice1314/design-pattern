package abstract_factory.furniture.impl;

import abstract_factory.furniture.Chair;

public class ArtDecoChair implements Chair {
    @Override
    public void hasLegs() {
        System.out.println("这是装饰风艺术风格的椅子");
        System.out.println("这种椅子有四条腿");
    }

    @Override
    public void sitOn() {
        System.out.println("这是装饰风艺术风格的椅子");
        System.out.println("这种椅子可以坐");
    }
}
