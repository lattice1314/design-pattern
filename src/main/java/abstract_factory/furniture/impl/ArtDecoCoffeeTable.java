package abstract_factory.furniture.impl;

import abstract_factory.furniture.CoffeeTable;

public class ArtDecoCoffeeTable implements CoffeeTable {
    @Override
    public void hasLegs() {
        System.out.println("这是装饰风艺术风格的茶几");
        System.out.println("这种茶几没有腿");
    }

    @Override
    public void layUp() {
        System.out.println("这是装饰风艺术风格的茶几");
        System.out.println("这种茶几可以放东西");
    }
}
