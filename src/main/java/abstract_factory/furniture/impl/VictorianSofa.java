package abstract_factory.furniture.impl;

import abstract_factory.furniture.Sofa;

public class VictorianSofa implements Sofa {
    @Override
    public void hasLegs() {
        System.out.println("这是维多利亚风格的沙发");
        System.out.println("这种沙发有四条腿");
    }

    @Override
    public void lieDown() {
        System.out.println("这是维多利亚风格的沙发");
        System.out.println("这种沙发可以躺");
    }
}
