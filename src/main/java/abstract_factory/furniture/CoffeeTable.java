package abstract_factory.furniture;

public interface CoffeeTable {

    void hasLegs();

    void layUp();

}
