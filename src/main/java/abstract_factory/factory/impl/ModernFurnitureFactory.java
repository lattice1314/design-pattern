package abstract_factory.factory.impl;

import abstract_factory.factory.FurnitureFactory;
import abstract_factory.furniture.Chair;
import abstract_factory.furniture.CoffeeTable;
import abstract_factory.furniture.Sofa;
import abstract_factory.furniture.impl.ModernChair;
import abstract_factory.furniture.impl.ModernCoffeeTable;
import abstract_factory.furniture.impl.ModernSofa;

public class ModernFurnitureFactory implements FurnitureFactory {
    @Override
    public Chair createChair() {
        return new ModernChair();
    }

    @Override
    public CoffeeTable createCoffeeTable() {
        return new ModernCoffeeTable();
    }

    @Override
    public Sofa createSofa() {
        return new ModernSofa();
    }
}
