package abstract_factory.factory.impl;

import abstract_factory.factory.FurnitureFactory;
import abstract_factory.furniture.Chair;
import abstract_factory.furniture.CoffeeTable;
import abstract_factory.furniture.Sofa;
import abstract_factory.furniture.impl.VictorianChair;
import abstract_factory.furniture.impl.VictorianCoffeeTable;
import abstract_factory.furniture.impl.VictorianSofa;

public class VictorianFurnitureFactory implements FurnitureFactory {
    @Override
    public Chair createChair() {
        return new VictorianChair();
    }

    @Override
    public CoffeeTable createCoffeeTable() {
        return new VictorianCoffeeTable();
    }

    @Override
    public Sofa createSofa() {
        return new VictorianSofa();
    }
}
