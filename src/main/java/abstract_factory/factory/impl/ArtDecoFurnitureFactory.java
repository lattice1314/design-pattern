package abstract_factory.factory.impl;

import abstract_factory.factory.FurnitureFactory;
import abstract_factory.furniture.Chair;
import abstract_factory.furniture.CoffeeTable;
import abstract_factory.furniture.Sofa;
import abstract_factory.furniture.impl.ArtDecoChair;
import abstract_factory.furniture.impl.ArtDecoCoffeeTable;
import abstract_factory.furniture.impl.ArtDecoSofa;

public class ArtDecoFurnitureFactory implements FurnitureFactory {
    @Override
    public Chair createChair() {
        return new ArtDecoChair();
    }

    @Override
    public CoffeeTable createCoffeeTable() {
        return new ArtDecoCoffeeTable();
    }

    @Override
    public Sofa createSofa() {
        return new ArtDecoSofa();
    }
}
